const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const { Schema } = mongoose;

const StudentSchema = Schema({
    studentName: { type: String, required: false},
    studentNumber: { type: String, required: true, index: { unique: true } },
    email: { type: String, required: false, index: { unique: true } },
    password: { type: String, required: false},
    NonHashedPassword: { type: String, required: false},
    phone: { type: String, required: false},
    level: { type: String, required: false},
    examsId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: true, unique: false, sparse: true, ref: 'Exam' },
    courseId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: true, unique: false, sparse: true, ref: 'Course' },
    marksId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: true, unique: false, sparse: true, ref: 'Mark' },
    markCourseId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: true, unique: false, sparse: true, ref: 'MarkCourse' },
    teacherId: { type: mongoose.Schema.Types.ObjectId, ref: 'Teacher', required: false },
    joined: { type: Date, default: new Date()},
});
StudentSchema.pre('save', async function (next) {
    //Check if password is not modified
    if (!this.isModified('password')) {
        return next();
    }
    const encryptedString = cryptr.encrypt(this.NonHashedPassword);

    //Encrypt the password
    try {
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(this.password, salt);
        this.password = hash;
        this.NonHashedPassword = encryptedString;
        next();
    } catch (e) {
        return next(e);
    }
});


StudentSchema.methods.toJSON = function () {
    const StudentObject = this.toObject();
    delete StudentObject.password;
    const decryptedString = cryptr.decrypt(StudentObject.NonHashedPassword);
    StudentObject.NonHashedPassword = decryptedString;
    return StudentObject;
};

StudentSchema.methods.isPasswordMatch = function (password, hashed, callback) {
    bcrypt.compare(password, hashed, (err, success) => {
        if (err) {
          return callback(err);
        }
        callback(null, success);
      });
  };
StudentSchema.plugin(autoIncrement.plugin,  {
    model: 'Student',
    field: 'studentNumber',
    startAt: (new Date()).getFullYear()+150,
    incrementBy: 1
})
const Student = mongoose.model('Student', StudentSchema);
module.exports = Student;