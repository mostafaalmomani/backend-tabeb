const TeacherSchema = require('../models/teacher.model');
exports.createSuperAdmin = () => {
    try {
        TeacherSchema.findOne({email:process.env.email.toLowerCase()}).then(docCount => {
            if(docCount!=null || docCount!= undefined){
                console.log('SuperAdmin already created ');
            } else {
                let defaultSuperAdmin = new TeacherSchema({
                    fullName: process.env.fullName,
                    email: process.env.email.toLowerCase(), 
                    password: process.env.password,
                    phoneNumber: process.env.phoneNumber,
                    purePassword: process.env.purePassword,
                });
        
                defaultSuperAdmin.save().then(doc => {
                    console.log('SuperAdmin created successfully ');
                })
            }
        });

    } catch (error) {

    }
};