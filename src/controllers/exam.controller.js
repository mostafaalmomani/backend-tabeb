const examController = {};
const Course = require('../models/course.model');
const Exam = require('../models/exam.model');
const Mark = require('../models/mark.model');
const Rules = require('../models/rules.model');
const Student = require('../models/student.model');
const MarkCourse = require('../models/mark.course.model');
const Questions = require('../models/questions.model');
const ExamStatus = require('../models/exam.status.model');
const Assessment = require('../models/assessment.course');
const Teacher = require('../models/teacher.model');
const StudentAnswers = require('../models/answersFile.model');
const Confirm = require('../models/confirm.request.model');

examController.create = async (req, res, next) => {

    try {
        const { examType, courseName, questionsCount,
            semester, courseNumber, year, examDate, start, end, duration, maxGreade, markForEachQ } = req.body.exam;
        const { returnBack, isRandomSequence, examLanguage,
            resultVisable, incorrectAnswersIsVisable, isRequiredQusetions, numberOfPage } = req.body.rules
        const newRules = new Rules({
            returnBack,
            isRandomSequence,
            examLanguage,
            resultVisable,
            incorrectAnswersIsVisable,
            isRequiredQusetions,
            numberOfPage
        })
        rules = await newRules.save();
        const newExam = new Exam({
            examType,
            courseName,
            questionsCount,
            semester,
            courseNumber,
            year,
            examDate,
            start,
            end,
            duration,
            maxGreade,
            markForEachQ,
            teacherId: req.userData._id,
            rulesId: rules._id
        });
        req.body.students.forEach(student => {
            Course.findOneAndUpdate({ courseNumber: courseNumber }, {
                $push: { studentsId: student._id }
            }).then(course => {
                Student.findOneAndUpdate({ _id: student._id }, {
                    $push: { courseId: course._id }
                }).then(docs => {
                })
            });
        });



        newExam.save().then(exam => {
            req.body.students.forEach(student => {
                Exam.findByIdAndUpdate(exam._id, {
                    $push: { studentsId: student._id }
                }).then(exam => {
                    Student.findOneAndUpdate({ _id: student._id }, {
                        $push: { examsId: exam._id }
                    }).then(docs => {
                        // console.log(docs)
                    })

                });
                const newExamStatus = new ExamStatus({
                    examId: exam._id,
                    studentId: student._id,
                    examDate: exam.examDate,

                });

                newExamStatus.save().then(status => {
                });

                // Course.findOneAndUpdate({ courseNumber: courseNumber }, {
                //     $push: { studentsId: student._id }
                // }).then(document => {
                //     // console.log(document)
                // });
            });
            res.status(200).json({ ok: 'ok', exam: exam, rules: rules, message: 'saved' });

        })
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getExams = async (req, res, next) => {
    try {
        Exam.find({ teacherId: req.userData._id }).then(exams => {
            Rules.find({}).then(rules => {
                res.status(200).json({ exams: exams, rules: rules });
            });
        }).catch(error => {
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getStudents = async (req, res, next) => {
    try {
        Exam.findById(req.params.exam_id).then(document => {
            Student
                .find()
                .where("_id")
                .in(document.studentsId)
                .exec((err, records) => {
                    res.status(200).json({
                        students: records
                    });
                });
        }).catch(error => {
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.saveOtherMark = async (req, res, next) => {
    try {

        var [lock,unlock] = newLock();
        var doneCount     = 0;
        const { mark } = req.body;
        const { courseNumber } = mark[0];
        Confirm.findOneAndUpdate({courseNumber: courseNumber}, {isConfirmed: false}).then(doc => {
            // console.log(doc);
            MarkCourse.updateMany({courseNumber: courseNumber},{isConfirmed: false}).then(doc => {
                // console.log(doc);
            })
        });
        mark.forEach(element => {
            if (element != null) {
                const { courseNumber, studentsId, markPoint, markDesc } = element;
                
                Mark.find({ studentsId: studentsId, courseNumber: courseNumber, markDesc: markDesc }).then(mark => {
                    // console.log(mark);
                    if (mark.length > 0) {
                        Mark.findOneAndRemove({ studentsId: studentsId, courseNumber: courseNumber, markDesc: markDesc }).then(doc => {
                        }).catch(e => {
                            console.log(e)
                            return res.status(500).send({ e: e });
                        });
                    } 
                        const newMark = new Mark({
                            markDesc,
                            studentsId,
                            markPoint,
                            courseNumber: courseNumber,
                            teacherId: req.userData._id,
                        });
                        newMark.save().then(mark => {
                            Student.findByIdAndUpdate(studentsId, {
                                $push: { marksId: mark._id }
                            }).then(doc => {
                            })
                        }).catch(error => {
                            res.status(500).json({ error });
                        });
                        updateAssessmentForOtherMark(studentsId, courseNumber, markPoint);
                    

                });

            }
            doneCount++;

        if (doneCount>=mark.length)
            unlock();
        });
        // await lock;
        return res.send({ message: "saved" });

    } catch (error) {

    }
}

examController.saveMark = async (req, res, next) => {

    try {
        const { examId, studentsId, markPoint, markDesc } = req.body.mark;
        Mark.find({ studentsId: studentsId, examId: examId }).then(mark => {
            if (mark.length > 0) {
                Mark.findOneAndUpdate({ studentsId: studentsId, examId: examId },
                    { markPoint: markPoint, markDesc: markDesc }, { new: true, upsert: true }).then(doc => {
                        Exam.findById(examId).then(currExam => {
                            updateAssessment(markDesc, markPoint, studentsId, currExam.courseNumber, currExam.maxGreade);
                        });
                        return res.send({ message: "saved", mark: doc });
                    }).catch(e => {
                        console.log(e)
                        return res.status(500).send({ e: e });
                    });
            } else {
                Exam.findById(examId).then(currExam => {
                    const newMark = new Mark({
                        examId,
                        studentsId,
                        markPoint,
                        markDesc,
                        courseNumber: currExam.courseNumber,
                        teacherId: req.userData._id,
                    });
                    newMark.save().then(mark => {
                        creatAssessment(currExam.courseNumber, markPoint, currExam.examType, studentsId, currExam.maxGreade);
                        Student.findByIdAndUpdate(studentsId, {
                            $push: { marksId: mark._id }
                        }).then(doc => {
                            res.status(200).json({
                                message: "saved",
                                mark: mark
                            });
                        })
                    }).catch(error => {
                        res.status(500).json({ error });
                    });
                })
            }
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.saveMarks = async (req, res, next) => {

    try {
        const { mark } = req.body;
        mark.forEach(element => {
            const { examId, studentsId, markPoint, markDesc } = element;
            Mark.find({ studentsId: studentsId, examId: examId }).then(mark => {
                if (mark.length > 0) {
                    Mark.findOneAndUpdate({ studentsId: studentsId, examId: examId },
                        { markPoint: markPoint, markDesc: markDesc }, { new: true, upsert: true }).then(doc => {
                            Exam.findById(examId).then(currExam => {
                                updateAssessment(markDesc, markPoint, studentsId, currExam.courseNumber, currExam.maxGreade);
                            });
                            // return res.send({ message: "saved", mark: doc });
                        }).catch(e => {
                            console.log(e)
                            return res.status(500).send({ e: e });
                        });
                } else {
                    Exam.findById(examId).then(currExam => {
                        const newMark = new Mark({
                            examId,
                            studentsId,
                            markPoint,
                            markDesc,
                            courseNumber: currExam.courseNumber,
                            teacherId: req.userData._id,
                        });
                        newMark.save().then(mark => {
                            creatAssessment(currExam.courseNumber, markPoint, currExam.examType, studentsId, currExam.maxGreade);
                            Student.findByIdAndUpdate(studentsId, {
                                $push: { marksId: mark._id }
                            }).then(doc => {

                            })
                        }).catch(error => {
                            res.status(500).json({ error });
                        });
                    })
                }
            });
        });
        res.status(200).json({
            message: "saved",
            mark: null
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getStudentsMark = async (req, res, next) => {
    try {
        const { exam_id, examType } = req.params;
        Mark.find({ examId: exam_id, markDesc: examType }).then(marks => {
            res.status(200).json({
                marks: marks
            });
        }).catch(error => {
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

function newLock(){
    var unlock,lock=new Promise((res,rej)=>{ unlock=res; });
    return [lock,unlock];
}

examController.saveMarkCourse = async (req, res, next) => {

    var [lock,unlock] = newLock();
    var doneCount     = 0;
    try {
        const { marks } = req.body;
        marks.forEach(element => {
            const { grade, studentsId, courseNumber, markPoint, courseName, examStatus } = element;
            MarkCourse.find({ courseNumber: courseNumber, studentsId: studentsId }).then(mark => {
                // console.log(mark)
                if (mark.length > 0) {
                    MarkCourse.findOneAndUpdate({ courseNumber: courseNumber, studentsId: studentsId }, { markPoint: markPoint, grade: grade, examStatus: examStatus }).then(doc => {
                        Assessment.findOneAndUpdate({ courseNumber: courseNumber, studentsId: studentsId }, { finalSum: grade })
                            .then(doc => { });
                    }).catch(e => {
                        return res.status(500).send({ e: e });
                    });
                } else {
                    const newMark = new MarkCourse({
                        grade,
                        studentsId,
                        markPoint,
                        examStatus,
                        courseNumber: courseNumber,
                        courseName: courseName,
                        teacherId: req.userData._id,
                    });
                    newMark.save().then(mark => {
                        Student.findByIdAndUpdate(studentsId, {
                            $push: { markCourseId: mark._id }
                        }).then(doc => {
                        })
                    }).catch(error => {
                        res.status(500).json({ error });
                    });
                    Assessment.findOneAndUpdate({ courseNumber: courseNumber, studentsId: studentsId }, { finalSum: grade })
                        .then(doc => { });

                }
            });
            doneCount++;
            
        if (doneCount>=marks.length)
            unlock();
        });
        // await lock;
        return res.status(200).json({ message: "saved" });

    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.createCourse = async (req, res, next) => {

    try {
        const { courseName, courseNumber, scales } = req.body.course;
        const newCourse = new Course({
            courseName, courseNumber, scales, teacherId: req.userData._id
        });
        newCourse.save().then(course => {
            res.status(200).json({
                message: "saved"
            });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};


examController.editCourse = async (req, res, next) => {

    try {

        const { courseName, courseNumber, scales, _id } = req.body.course;
        let updatedCourse = await Course.findByIdAndUpdate({ _id: _id });
        scales.forEach(s => {
            updatedCourse.scales.push(s);
        });
        updatedCourse.markModified('scales');
        updatedCourse.save()
            .then(doc => {
                console.table(doc.scales);
                    return res.send({ message: "saved!" });
                }).catch(e => {
                    console.log(e)
                    return res.status(500).send({ e: e });
                });

    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getCourses = async (req, res, next) => {
    try {
        Teacher.findById({ _id: req.userData._id }).populate({
            path: 'course_id',
            model: 'Course'
        }).then(courses => {
            res.status(200).json({
                courses: courses.course_id
            });
        }).catch(error => {
            console.log(error)
            res.status(500).json({ error: error })
        })
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
}


examController.getStudentCourseMark = async (req, res, next) => {
    const { student_id, course_id } = req.params;
    try {
        MarkCourse.findOne({ studentsId: student_id, courseNumber: course_id }).then(mark => {
            res.status(200).json({
                mark: mark
            });
        }).catch(error => {
            res.status(500).json({ error: error })
        })
    } catch (error) {
        res.status(500).json({ error: error })
    }
}
examController.getCoursesStudents = async (req, res, next) => {
    try {
        const { course_number } = req.params;
        Course.findOne({ courseNumber: course_number }).then(document => {
            Teacher.findById(req.userData._id).then(teacherDoc => {
                Student
                .find()
                .where("_id")
                .in(teacherDoc.students)
                .exec((err, records) => {
                    res.status(200).json({
                        students: records,
                        courseName: document.courseName,
                        course: document
                    });
                });
            })
            
        }).catch(error => {
            console.log(error);
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getCoursesStudentsForConfirm = async (req, res, next) => {
    try {
        const { course_number } = req.params;
        Assessment.find({ courseNumber: course_number }).populate({
            path: 'course',
            model: 'Course'
        }).then(document => {
            const students = document.map(d => d.studentsId)
                Student
                .find()
                .where("_id")
                .in(students)
                .exec((err, records) => {
                    res.status(200).json({
                        students: records,
                        courseName: document.courseName,
                        course: document.course
                    });
                });
        }).catch(error => {
            console.log(error);
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

examController.getStudentMarks = async (req, res, next) => {
    try {
        const { studentId, courseNumber } = req.body;
        Mark.find({ teacherId: req.userData._id, courseNumber: courseNumber }).then(document => {
            res.status(200).json({ marks: document });
        }).catch(error => {
            res.status(500).json({ error });
        });
    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};


examController.getExamInfo = async (req, res, next) => {
    try {
        const { examId } = req.params;
        Exam.findById({ _id: examId })
            .populate({
                path: 'rulesId',
                model: 'Rules'
            }).then(exam => {
                res.status(200).send(exam);
            }).catch(error => {
                res.status(500).send(error);
            })
    } catch (error) {
        res.status(500).send(error);
    }
}

examController.saveAnswers = async (req, res, next) => {
    const answers = req.body.answers;
    const { examId } = req.params;
    let sum = 0;
    let questionsId = answers.map(obj => { return obj.qustion._id });
    let studentTextAnswers = answers.map(obj => { return obj.answer });

    await answers.forEach(async (answer) => {
        await Questions.updateOne({
            _id: answer.qustion._id,
            answers: { $elemMatch: { text: answer.answer } }
        },
            { $inc: { "answers.$.timesAnswered": 1, total: 1 } }
        )
    })


    Questions.find().select('examId correctAnswer.text point')
        .where('_id').in(questionsId).then(questions => {
            questions.forEach(q => {
                if (studentTextAnswers.includes(q.correctAnswer[0].text)) {
                    sum += q.point;
                }
            });

            Exam.findById(examId).then(exam => {
                const newMark = new Mark({
                    markDesc: exam.examType,
                    examId,
                    courseNumber: exam.courseNumber,
                    studentsId: req.userData._id,
                    markPoint: sum,
                    created: new Date(),
                    teacherId: exam.teacherId,
                });
                newMark.save().then(result => {
                    ExamStatus.findOneAndUpdate({ studentId: req.userData._id, examId: examId }, { isActive: false }).then(document => {
                        creatAssessment(result.courseNumber, result.markPoint, exam.examType, req.userData._id, exam.maxGreade);
                        res.status(200).send({ resp: sum, _id: result._id });
                    });
                })
            });

        });


}
examController.confirmAssement = async (req, res, next) => {
    const { confirmAssementObj, examType, courseNumber } = req.body;
    Assessment.updateMany({ courseNumber: courseNumber }, confirmAssementObj, { multi: true, upsert: true }).then(document => {
        res.status(200).send({ Confirmed: true });
    }).catch(error => {
        res.status(500).send(error);
    })
}

examController.getMark = async (req, res, next) => {
    const { markId } = req.params;
    Mark.findById({ _id: markId }).then(mark => {
        Exam.findById({ _id: mark.examId })
            .populate({
                path: 'rulesId',
                model: 'Rules'
            }).then(exam => {
                res.status(200).send({ rules: exam.rulesId, mark: mark, maxGreade: exam.maxGreade });
            }).catch(error => {
                res.status(500).send(error);
            })
    }).catch(error => {
        res.status(500).send(error);
    })
}

examController.saveAnswersFile = async (req, res, next) => {
    const blobObj = req.files.Answers;
    const filePath = blobObj.path.substr(blobObj.path.lastIndexOf('/') + 1);
    const { examId, courseNumber } = req.body;
    try {
        const newStudentAnswers = new StudentAnswers({
            examId,
            courseNumber,
            path: filePath,
            studentsId: req.userData._id
        });

        newStudentAnswers.save().then(doc => {
            res.status(200).send({ message: 'saved' })
        })
    } catch (error) {
        res.status(500).send(error);
    }
}

async function updateAssessmentForOtherMark(studentsId, courseNumber, markPoint) {
    let assessmentDoc = await Assessment.findOneAndUpdate({ studentsId: studentsId, courseNumber: courseNumber }, { new: true, upsert: true });
    assessmentDoc.sum = Number.parseFloat((assessmentDoc.sum - assessmentDoc.otherExamMark) + Number.parseFloat(markPoint));
    assessmentDoc.markModified('sum');
    assessmentDoc.otherExamMark = Number.parseFloat(markPoint);
    assessmentDoc.markModified('otherExamMark');
    assessmentDoc.finalSum = Number.parseFloat(assessmentDoc.sum + assessmentDoc.finalExamMark);
    assessmentDoc.markModified('finalSum');
    await assessmentDoc.save();
    MarkCourse.findOneAndUpdate({ studentsId: studentsId, courseNumber: courseNumber },
        {grade: assessmentDoc.finalSum}).then(doc => {

        }).catch(err => console.log(err));

}

async function updateAssessment(markDesc, markPoint, studentsId, courseNumberArg, maxGreade) {
    const markDescObject = {};
    markDesc = Number.parseFloat(markDesc);
    switch (markDesc) {
        case 1:
            markDescObject['firstExamMark'] = markDesc === 1 ? markPoint : -1;
            markDescObject['firstExam'] = markDesc === 1 ? maxGreade : 0;
            break;
        case 2:
            markDescObject['secondExamMark'] = markDesc === 2 ? markPoint : -1;
            markDescObject['secondExam'] = markDesc === 2 ? maxGreade : 0;
            break;
        case 3:
            markDescObject['finalExamMark'] = markDesc === 3 ? markPoint : -1;
            break;
        case 4:
            markDescObject['otherExamMark'] = markDesc === 4 ? markPoint : 0;
            break;
        default:
            break;
    }

    Assessment.findOneAndUpdate({ studentsId: studentsId, courseNumber: courseNumberArg },
        markDescObject, { new: true, upsert: true }).then(doc => {
            calculateSum(doc, studentsId, courseNumberArg);
        });

}

async function calculateSum(doc, studentsId, courseNumber) {
    try {
        if (doc != null) {
            // if (doc.firstExamMark != 0 && doc.secondExamMark != 0) {
                
                let assessmentDoc = await Assessment.findOneAndUpdate({ studentsId: studentsId, courseNumber: courseNumber }, { new: true, upsert: true });
                assessmentDoc.secondExamMark = assessmentDoc.secondExamMark < 0 ? 0:assessmentDoc.secondExamMark;
                assessmentDoc.firstExamMark = assessmentDoc.firstExamMark < 0 ? 0:assessmentDoc.firstExamMark;
                assessmentDoc.otherExamMark = assessmentDoc.otherExamMark < 0 ? 0:assessmentDoc.otherExamMark;
                assessmentDoc.sum = Number.parseFloat(assessmentDoc.firstExamMark + assessmentDoc.secondExamMark + assessmentDoc.otherExamMark);
                assessmentDoc.finalSum = Number.parseFloat(assessmentDoc.sum + assessmentDoc.finalExamMark);
                await assessmentDoc.save();
                calculateAVG(courseNumber);
                findStudentOrder(assessmentDoc.studentsId, courseNumber)
            // }
        }

    } catch (error) {
        console.log(error);
    }

}

function calculateAVG(courseNumber) {
    Assessment.find({ courseNumber: courseNumber }).then(studedntsAssessment => {
        let sum = 0;
        let numberOfStudents = studedntsAssessment.length;
        studedntsAssessment.forEach(ass => {
            sum = sum + ass.sum;
        });

        let avg = (sum / numberOfStudents) < 0 ? (sum / numberOfStudents) * -1 : (sum / numberOfStudents);
        Assessment.updateMany({ courseNumber: courseNumber }, { avg: Number.parseFloat(avg).toFixed(2) }, { multi: true, upsert: true })
            .then(doc => {
            })
    });
}

async function creatAssessment(courseNumberArg, markPoint, examType, studentsId, maxGreade) {

    Assessment.findOne({ studentsId: studentsId, courseNumber: courseNumberArg }).then(assessments => {
        if (assessments === null) {
            Course.findOne({ courseNumber: courseNumberArg }).then(doc => {
                const newAssessment = new Assessment({
                    courseName: doc.courseName,
                    courseNumber: courseNumberArg,
                    course: doc._id,
                    firstExamMark: examType === 1 ? markPoint : -1,
                    firstExam: examType === 1 ? maxGreade : 0,
                    secondExamMark: examType === 2 ? markPoint : -1,
                    secondExam: examType === 2 ? maxGreade : 0,
                    otherExamMark: examType === 4 ? markPoint : 0,
                    order: 0,
                    avg: examType === 3 ? markPoint : -1,
                    studentsId: studentsId
                })
                newAssessment.save().then(doc => {
                    calculateSum(doc, studentsId);
                })
            });
        } else {
            updateAssessment(examType, markPoint, studentsId, courseNumberArg, maxGreade);
        }
    })
}

async function findStudentOrder(student, courseNumber) {
    let studentOrder = 0;
    Assessment.find({ courseNumber: courseNumber }).sort({ sum: -1 }).then(elements => {
        let order = 1;
        elements.forEach(s => {
            Assessment.findOneAndUpdate({ _id: s._id }, { order: order },
                { new: true, upsert: true }).then(doc => {
                });
            order++;
        })
    })
}

function getStudents(exam, callback) {
    Student
        .find()
        .where("_id")
        .in(exam.studentsId)
        .exec((err, records) => {
            callback(records)
        });
}
module.exports = examController;