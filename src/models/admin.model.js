const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const { Schema } = mongoose;

const AdminSchema = Schema({
  fullName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  purePassword: { type: String, required: false },
  phoneNumber: { type: String, required: true },
  joined: { type: Date, default: (new Date()).getDate() },
  resetPasswordLink: {
    data: String,
    default: ''
  }
});
AdminSchema.pre('save', async function (next) {
  //Check if password is not modified
  if (!this.isModified('password')) {
    return next();
  }
  const encryptedString = cryptr.encrypt(this.purePassword);

  //Encrypt the password
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    this.purePassword = encryptedString;
    next();
  } catch (e) {
    return next(e);
  }
});

AdminSchema.methods.isPasswordMatch = function (password, hashed, callback) {
  bcrypt.compare(password, hashed, (err, success) => {
    if (err) {
      return callback(err);
    }
    callback(null, success);
  });
};

AdminSchema.methods.toJSON = function () {
  const AdminObject = this.toObject();
  delete AdminObject.password;
  const decryptedString = cryptr.decrypt(TeacherObject.purePassword);
  TeacherObject.purePassword = decryptedString;
  return AdminObject;
};

const Admin = mongoose.model('Admin', AdminSchema);
module.exports = Admin;