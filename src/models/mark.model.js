const mongoose = require('mongoose');

const { Schema } = mongoose;

const MarkSchema = Schema({
    markDesc: { type: String, required: false, default: 'def' },
    examId: { type: mongoose.Schema.Types.ObjectId, required: false },
    courseNumber: { type: String, required: true },
    studentsId: { type: mongoose.Schema.Types.ObjectId, required: true },
    markPoint: { type: Number, required: false },
    created: { type: Date, default: new Date() },
    teacherId: { type: mongoose.Schema.Types.ObjectId, required: true }
});

const Mark = mongoose.model('Mark', MarkSchema);
module.exports = Mark;