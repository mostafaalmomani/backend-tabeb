const mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);
const { Schema } = mongoose;

const CourseSchema = Schema({
    courseName: { type: String, required: false },
    courseNumber: { type: String, required: false },
    scales: {type: Object, unique: false, required: false},
    created: { type: Date, default: new Date() },
    studentsId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: true, unique: true, sparse: true, ref: 'Student' },
    teacherId: { type: mongoose.Schema.Types.ObjectId, required: true }
});

CourseSchema.plugin(autoIncrement.plugin,  {
    model: 'Course',
    field: 'courseNumber',
    startAt: (new Date()).getUTCFullYear()+300*3,
    incrementBy: 2
})
const Course = mongoose.model('Course', CourseSchema);
module.exports = Course;


//     examId: { type: mongoose.Schema.Types.ObjectId, required: true }, 
//     studentsId: { type: mongoose.Schema.Types.ObjectId, required: true },
//     CoursePoint: { type: Date, required: false },