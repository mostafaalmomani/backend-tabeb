const express = require('express');
const router = express.Router();
const passport = require('passport');
const teacherController = require('../controllers/teacher.controller');
const questionController = require('../controllers/question.controller');
const studentController = require('../controllers/student.controller');
const examController = require('../controllers/exam.controller');
const testBankController = require('../controllers/test.bank.controller');
const adminController = require('../controllers/admin.controller');
const imageController = require('../controllers/file.reader.controller');

const multipart = require('connect-multiparty');
const multipartMiddleware = multipart({ uploadDir: __dirname+ '/../uploads/question' });

const checkAuth = require('../config/passport');
router.get('/session/expired', (req, res, next) => {
    res.send({ active: req.session })
});

const validatePayloadMiddleware = (req, res, next) => {
    if (req.body) {
        next();
    } else {
        res.status(403).send({
            errorMessage: 'You need a payload'
        });
    }
};


// Auth and Sign Up
router.post('/auth/student', validatePayloadMiddleware,studentController.login);

// ---------------------------- teacher public Routes --------------------------------
// router.post('/register', checkAuth, teacherController.regisetr);
router.post('/auth', validatePayloadMiddleware, teacherController.login);
router.post('/auth/admin', validatePayloadMiddleware, adminController.login);
router.post('/forgetPassword', validatePayloadMiddleware, teacherController.forgetPassword);
router.put('/resetPassword', validatePayloadMiddleware, teacherController.resetPassword);

router.get('/logout', function (req, res) {
    res.status(200).send({ isLoggedOut: true });
});

//------------------- session config --------------------------------------
const authMiddleware = (req, res, next) => {
    console.log(req.isAuthenticated())
    
    if (req.isAuthenticated()) {
        next();
    } else {
        res.status(403).send({
            errorMessage: 'You must be logged in.'
        });
    }
};
// -------------- Protected Routes ----------------- //
router.post('/confirmMark', checkAuth, teacherController.confirm);
router.post('/getAssessments',checkAuth, teacherController.getAssessments);
router.get('/getAllStudents', checkAuth, teacherController.getAllStudents);
router.post('/setStudents', checkAuth, teacherController.setStudents)
// -------------- exam routs ----------------- //
router.post('/createExam',checkAuth, examController.create);
router.post('/getExam',checkAuth, examController.getExams);
router.get('/getStudents/:exam_id',checkAuth, examController.getStudents);
router.post('/saveExamMarks',checkAuth, examController.saveMarks);
router.post('/saveExamMark',checkAuth, examController.saveMark);
router.get('/getStudentsMark/:exam_id/:examType',checkAuth, examController.getStudentsMark);
router.post('/createCourse',checkAuth, examController.createCourse);
router.post('/editCourse',checkAuth, examController.editCourse);
router.get('/getCourse',checkAuth, examController.getCourses);
router.get('/getCoursesStudents/:course_number',checkAuth, examController.getCoursesStudents);
router.get('/getCoursesStudentsForConfirm/:course_number',checkAuth, examController.getCoursesStudentsForConfirm);
router.post('/getStudentMarks',checkAuth, examController.getStudentMarks);
router.post('/saveCourseMark',checkAuth, examController.saveMarkCourse);
router.get('/getStudentCourseMark/:student_id/:course_id',checkAuth, examController.getStudentCourseMark);
router.post('/saveOtherMark',checkAuth, examController.saveOtherMark);
router.post('/confirmAssement',checkAuth, examController.confirmAssement);


// -------------------- question -------------------------------------------
router.post('/saveQuestion', checkAuth,multipartMiddleware, questionController.create);

// ------------------- student routs ---------------------------------------
router.post('/register/student', checkAuth, studentController.regisetr);
router.get('/getstudents',checkAuth,studentController.get);
router.get('/getstudents',checkAuth,studentController.get);
router.get('/getAllForAdmin',checkAuth,studentController.getAllForAdmin);
router.post('/updatestudents',checkAuth,studentController.update);
router.delete('/deletestudents/:student_id',checkAuth,studentController.delete);
router.get('/getStudentsFileAnswers/:examId',checkAuth,studentController.getStudentsFileAnswers);
//------------------------test bank ---------------------------------- 
router.post('/createTest',checkAuth, testBankController.create);
router.post('/saveTestBankQuestion', checkAuth,multipartMiddleware, testBankController.createQ);
// router.get('/testJoin',  studentController.getAcademicStudentInformation);
//----------------------- admin routes -------------------------------
router.get('/cardInformation',checkAuth, adminController.getCardInformation);
router.get('/getCourses',checkAuth, adminController.getCourses);
router.get('/getTeachers', checkAuth, adminController.getTeachers);
router.post('/register', checkAuth, adminController.regisetr);
router.post('/updatesteacher',checkAuth, adminController.update);
router.delete('/deleteteacher/:teacher_id',checkAuth,adminController.delete);
router.get('/readImage/:path',checkAuth,imageController.getStudentAnswers); 
router.post('/confirm', checkAuth, adminController.confirm); 
router.get('/requests', checkAuth, adminController.getRequests); 
router.get('/deleteAllData', adminController.deleteAllData);
router.delete('/deleteCourse/:courseId',checkAuth, adminController.deleteCourse);
// router.post('/question', authMiddleware, questionController.create);
// router.post('/fileUpload', authMiddleware, questionController.fileUpload);
// router.put('/question/:question_id', authMiddleware, questionController.update);
// router.delete('/question/:question_id', authMiddleware, questionController.destroy);

module.exports = router;