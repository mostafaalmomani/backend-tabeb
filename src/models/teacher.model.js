const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const autoIncrement = require('mongoose-auto-increment');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
 

autoIncrement.initialize(mongoose.connection);
const { Schema } = mongoose;

const TeacherSchema = Schema({
  fullName: { type: String, required: true },
  email: { type: String, required: true, unique: true },
  teacherNumber: { type: String, required: true, index: { unique: true } },
  password: { type: String, required: true },
  purePassword: { type: String, required: false },
  course_id: { type: [mongoose.Schema.Types.ObjectId], required: false, ref: 'Course' },
  students: { type: [mongoose.Schema.Types.ObjectId], required: false, ref: 'Student' },
  phoneNumber: { type: String, required: true },
  joined: { type: Date, default: (new Date()).getDate() },
  resetPasswordLink: {
    data: String,
    default: ''
  }
});
TeacherSchema.pre('save', async function (next) {
  //Check if password is not modified
  if (!this.isModified('password')) {
    return next();
  }

  //Encrypt the passwordconst 
const encryptedString = cryptr.encrypt(this.purePassword);
 
  try {
    const salt = await bcrypt.genSalt(10);
    const hash = await bcrypt.hash(this.password, salt);
    this.password = hash;
    this.purePassword = encryptedString;
    next();
  } catch (e) {
    return next(e);
  }
});

TeacherSchema.methods.isPasswordMatch = function (password, hashed, callback) {
  bcrypt.compare(password, hashed, (err, success) => {
    if (err) {
      return callback(err);
    }
    callback(null, success);
  });
};

TeacherSchema.methods.toJSON = function () {
  const TeacherObject = this.toObject();
  delete TeacherObject.password;
  const decryptedString = cryptr.decrypt(TeacherObject.purePassword);
  TeacherObject.purePassword = decryptedString;
  return TeacherObject;
};

TeacherSchema.plugin(autoIncrement.plugin,  {
  model: 'Teacher',
  field: 'teacherNumber',
  startAt: (new Date()).getUTCFullYear()+300*2,
  incrementBy: 2
})
const Teacher = mongoose.model('Teacher', TeacherSchema);
module.exports = Teacher;