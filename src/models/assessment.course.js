const mongoose = require('mongoose');

const { Schema } = mongoose;

const AssessmentSchema = Schema({
    courseName: { type: String, required: false },
    courseNumber: { type: String, required: false },
    course: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Course' },
    firstExamMark: {type: Number, required: false, default: 0},
    firstExam: {type: Number, required: false, default: 0},
    secondExamMark: {type: Number, required: false, default: 0},
    secondExam: {type: Number, required: false, default: 0},
    finalExamMark: {type: Number, required: false, default: 0},
    otherExamMark: {type: Number, required: false, default: 0},
    order: {type: Number, required: false},
    avg: {type: Number, unique: false, required: false},
    sum: {type: Number, unique: false, required: false,default: 0},
    finalSum: {type: Number, unique: false, required: false,default: 0},
    examStatus: { type: String, required: false ,default: 'pass'},
    created: { type: Date, default: (new Date()) },
    isFirstConfirmed: { type: Boolean, default:false },
    isSecondConfirmed: { type: Boolean, default:false },
    studentsId: { type: mongoose.Schema.Types.ObjectId, required: true, index: true, unique: false, sparse: true, ref: 'Student' },
    teacherId: { type: mongoose.Schema.Types.ObjectId, required: false }
});
const Assessment = mongoose.model('Assessment', AssessmentSchema);
module.exports = Assessment;