const jwt = require('jsonwebtoken');
const Teacher = require("../models/teacher.model");
const MarkCourse = require("../models/mark.course.model");
const ConfirmSchema = require('../models/confirm.request.model');
const sgMail = require('@sendgrid/mail');
const { validationResult } = require('express-validator');
const _ = require('lodash');
const fs = require('fs');
const Course = require('../models/course.model');
const SENDGRID_API_KEY = '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'
const Assessment = require('../models/assessment.course');
const Student = require("../models/student.model");

const teacherController = {};

teacherController.regisetr = async (req, res, next) => {
    try {
        let filePath = 'src' + '\\uploads\\profile image' + req.files.uploads[0].path.substr(req.files.uploads[0].path.lastIndexOf('\\'));
        const { fullName, email, password, joined, phoneNumber } = req.body;
        const newteacher = new Teacher({
            fullName,
            email,
            password,
            phoneNumber,
            picture: filePath,
            joined
        });
        const teacher = await newteacher.save();
        return res.send({ teacher });
    } catch (e) {
        if (e.code === 11000 && e.name === 'MongoError') {
            const error = new Error(`Email address ${newteacher.email} is already taken`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};


teacherController.confirm = async (req, res, next) => {   

    try {
        const {courseNumber} = req.body;
        Teacher.findOne({_id: req.userData._id}).then(teacher => {
            Course.findOne({courseNumber: courseNumber}).then(doc => {
                const newRequest = new ConfirmSchema({
                    teacherId: req.userData._id,
                    courseNumber: courseNumber,
                    courseName: doc.courseName,
                    teacherName: teacher.fullName,
                    isConfirmed: false
                });
    
                newRequest.save().then(respons => {
                     res.status(200).send({Confirmed: true});
                }).catch(error => {
                     res.status(500).send(error);
                });
            });
        });
        
        
    } catch (error) {
        return res.status(500).json(error);
    }
   
}

teacherController.getAssessments = async (req, res, next) => {
    try {
        
        const {course} = req.body;
        Assessment.find({courseNumber: course}).populate(
            [{path: 'studentsId', model: 'Student'}, 
            {path: 'course', model: 'Course'}]).then(doc => {
            res.status(200).send({assessments: doc});
        }).catch(error => {
            console.log(error)
            return res.status(500).json(error);
        });
    } catch (error) {
        console.log(error)
        return res.status(500).json(error);
    }
}


teacherController.login = async (req, res, next) => {   
    try {
        const { email, password } = req.body;
        const teacher = await Teacher.findOne({ email });
        if (!teacher) {
            const err = new Error(`The email ${email} was not found on our system`);
            err.status = 401;
            return next(err);
        }

        //Check the password
        teacher.isPasswordMatch(password, teacher.password, (err, matched) => {
            if (matched) { //Generate JWT
                const secret = process.env.JWT_SECRET;
                const expire = process.env.JWT_EXPIRATION;

                const token = jwt.sign({ _id: teacher._id }, secret, { expiresIn: expire });
                res.cookie('teacherid',teacher._id)
                return res.send({
                    id: teacher._id,
                    teachername: teacher.email,
                    fullName: teacher.fullName,
                    token: token,
                    expiresIn: 3600
                });
            }
            res.status(401).send({
                error: 'Invalid email/password combination'
            });

        });

    } catch (e) {
        next(e);
    }

};
teacherController.forgetPassword = async (req, res, next) => {
    const { email } = req.body;
    const teacher = await teacher.findOne({ email });
    const secret = process.env.JWT_SECRET;
    const expire = process.env.JWT_EXPIRATION;
    const token = jwt.sign({ _id: teacher._id }, secret, { expiresIn: '5m' });
    sgMail.setApiKey(SENDGRID_API_KEY);
    const msg = {
        to: email,
        from: 'mostafaalmomani98@gmail.com',
        subject: `Password Reset link`,
        html: `
                    <h1 style="color: #61dafb">Please use the following link to reset your password</h1>
                    <p style="color: #61dafb">${process.env.CLIENT_URL}/teachers/password/reset/${token}</p>
                    <hr />
                    <p style="color: #61dafb">This email may contain sensetive information</p>
                    <p style="color: #61dafb">${process.env.CLIENT_URL}</p>
                `,
    };
    return teacher.updateOne(
        {
            resetPasswordLink: token
        },
        (err, success) => {
            if (err) {
                console.log('RESET PASSWORD LINK ERROR', err);
                return res.status(400).json({
                    error:
                        'Database connection error on teacher password forgot request'
                });
            } else {
                sgMail
                    .send(msg)
                    .then(sent => {
                        // console.log('SIGNUP EMAIL SENT', sent)
                        return res.json({
                            message: `Email has been sent to ${email}. Follow the instruction to activate your account`
                        });
                    })
                    .catch(err => {
                        // console.log('SIGNUP EMAIL SENT ERROR', err)
                        return res.json({
                            message: err.message
                        });
                    });
            }
        }
    );
    // return res.send({ message: 'check your email to reset your password' });

}

teacherController.getAllStudents = (req, res, next) => {
    try {
        Student.find({}).then(students => {
            return res.status(200).json({ students: students });
        }).catch(error => {
            return res.status(500).json(error);
        })
    } catch (error) {

    }
}

teacherController.setStudents = (req, res, next) => {
    try {
        const {students} = req.body;
        students.forEach(s => {
            Teacher.findOneAndUpdate({_id: req.userData._id} ,{
                $push: { students: s._id }
            }).then(students => {
            }).catch(error => {
                return res.status(500).json(error);
            })
        });
        return res.status(200).json({ message: 'saved!' });
    } catch (error) {
        return res.status(500).json(error);
    }
}

teacherController.resetPassword = (req, res, next) => {
    const { resetPasswordLink, newPassword } = req.body;
    console.log(resetPasswordLink)
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        const firstError = errors.array().map(error => error.msg)[0];
        return res.status(422).json({
            errors: firstError
        });
    } else {
        if (resetPasswordLink) {
            jwt.verify(resetPasswordLink, process.env.JWT_SECRET, function (
                err,
                decoded
            ) {
                if (err) {
                    return res.status(400).json({
                        error: 'Expired link. Try again'
                    });
                }
                teacher.findOne({ resetPasswordLink }).then(teacher => {

                });
                teacher.findOne(
                    {
                        resetPasswordLink
                    },
                    (err, teacher) => {
                        if (err || !teacher) {
                            return res.status(400).json({
                                error: 'Something went wrong. Try later'
                            });
                        }

                        const updatedFields = {
                            password: newPassword,
                            resetPasswordLink: ''
                        };

                        teacher = _.extend(teacher, updatedFields);

                        teacher.save((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    error: 'Error resetting teacher password'
                                });
                            }
                            res.json({
                                message: `Great! Now you can login with your new password`
                            });
                        });
                    }
                );
            });
        }
    }
}
teacherController.me = (req, res, next) => {
    const { teacher } = req.session;
    const contents = fs.readFileSync(filePath, { encoding: 'base64' });
    console.log(req.session)
    return res.status(200).json({ teacher })
}
module.exports = teacherController;