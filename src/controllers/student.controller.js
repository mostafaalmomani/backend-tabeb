const jwt = require('jsonwebtoken');
const Student = require("../models/student.model");
const sgMail = require('@sendgrid/mail');
const SENDGRID_API_KEY = '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'
const MarkCourse = require('../models/mark.course.model');
const Course = require('../models/course.model');
const Marks = require('../models/mark.model');
const Exam = require('../models/exam.model');
const Questions = require('../models/questions.model');
const ExamStatus = require('../models/exam.status.model');
const Assessment = require('../models/assessment.course');
const StudentAnswers = require('../models/answersFile.model');
const Teacher = require('../models/teacher.model');
const Admin = require("../models/admin.model");
const bcrypt = require('bcryptjs');
const Cryptr = require('cryptr');
const cryptr = new Cryptr('myTotalySecretKey');
const studentController = {};

studentController.regisetr = async (req, res, next) => {
    try {
        const {students} = req.body;
        students.forEach(element => {
        const { studentName, email, password, studentNumber, level, joined, phone } = element;
        const newstudent = new Student({
            studentName,
            email,
            password,
            NonHashedPassword: password,
            studentNumber,
            level,
            joined,
            phone,
            teacherId: req.userData._id
        });
       newstudent.save().then(doc => {
           
       });
    });
        return res.send({ message: "saved!" });
    } catch (e) {
        console.log(e)
        if (e.code === 11000 && e.name === 'MongoError') {
            //${req.body.student.email} 
            const error = new Error(`Email or Number  is already taken`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }
    
    }


};

studentController.update = async (req, res, next) => {
    try {
        const { studentName, email, password, studentNumber, level, joined, _id } = req.body.student;
        const salt = await bcrypt.genSalt(10);
        const hash = await bcrypt.hash(password, salt);
        const updateUserDto = req.body.student;
        updateUserDto['NonHashedPassword'] =  cryptr.encrypt(password);
        updateUserDto.password = hash;

        Student.findByIdAndUpdate(_id, updateUserDto).then(doc => {
            return res.send({ message: "saved!" });
        }).catch(e => {
            console.log(e)
            return res.status(500).send({ e: e });
        });

    } catch (e) {
        console.log(e)
        if (e.code === 11000 && e.name === 'MongoError') {
            const error = new Error(`Email or Number  ${req.body.student.email} is already taken`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};


studentController.delete = async (req, res, next) => {
    try {
        const { student_id } = req.params;
        Teacher.findByIdAndUpdate({ _id: req.userData._id },
            {
                $pull: {
                    students: student_id,
                },
            }).then(doc => {
            return res.send({ message: "deleted!" });
        }).catch(e => {
            console.log(e)
            return res.status(500).send({ e: e });
        });

    } catch (e) {
        console.log(e)
        if (e) {
            const error = new Error(`something error!!!`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};


studentController.login = async (req, res, next) => {
    try {
        const { studentNumber, password, email } = req.body;
        const student = await Student.findOne({ studentNumber });
        if (student==null) {
            const err = new Error(`The student Number ${studentNumber} was not found on our system`);
            err.status = 401;
            console.log(err);
            return next(err);
        }

        debugger;
        student.isPasswordMatch(password, student.password, (err, matched) => {
            if (matched) {
                const secret = process.env.JWT_SECRET;
                const expire = process.env.JWT_EXPIRATION;
                const token = jwt.sign({ _id: student._id }, secret, { expiresIn: expire });
                res.cookie('student', student._id)
                return res.send({
                    id: student._id,
                    student: student.email,
                    studentName: student.studentName,
                    token: token,
                    expiresIn: 6900,
                    studentNumber: student.studentNumber
                });
            }
            res.status(401).send({
                error: 'Invalid email/password combination'
            });
        });


    } catch (e) {
        next(e);
    }

};

studentController.get = (req, res, next) => {
    try {

        Teacher.findById(req.userData._id).then(document => {
            Student
                .find()
                .where("_id")
                .in(document.students)
                .exec((err, records) => {
                    res.status(200).json({
                        students: records
                    });
                });
        }).catch(error => {
            res.status(500).json({ error });
        });
    } catch (error) {

    }
}

studentController.getAllForAdmin = (req, res, next) => {
    try {
        Student
            .find({teacherId: req.userData._id}).then(students => {
                res.status(200).json({
                    students: students
                });
            }).catch(error => {
                res.status(500).json({ error });
            });
       
    } catch (error) {

    }
}

studentController.getCoursesMark = (req, res, next) => {
    try {
        MarkCourse.find({ studentsId: req.userData._id,isConfirmed: true }).then(Markcourses => {
            return res.status(200).json({ Markcourses: Markcourses });
        }).catch(error => {
            return res.status(500).json(error);
        })
    } catch (error) {
        return res.status(500).json(error);
    }
}

studentController.getCourseMark = (req, res, next) => {
    const { studentId } = req.params;
    try {
        MarkCourse.find({ studentsId: studentId }).then(marks => {
            res.send({ marks: marks });
        }).catch(err => res.status(500).send({ err: err }))
    } catch (error) {
        res.status(500).send({ error: error })
    }
}

studentController.getCourses = (req, res, next) => {
    const { courseNumber } = req.params;
    try {
        Course.find({ courseNumber: courseNumber }).then(courses => {
            res.send({ course: courses });
        });
    } catch (error) {

    }

}

studentController.getMarks = (req, res, next) => {
    const { courseNumber, studentId } = req.params;
    try {
        Marks.find({ courseNumber: courseNumber, studentsId: studentId }).then(marks => {
            res.send({ marks: marks });
        })
    } catch (error) {

    }
}

studentController.getExamsForStudent = (req, res, next) => {
    try {
        const { studentId } = req.params;
            ExamStatus.find({studentId: studentId, isActive: true }).then(status => {
                let examIsActive = [];
                examIsActive = status.map(statusId => {return statusId.examId.toString()});
                Exam.find().where('_id').in(examIsActive).then(exams => {
                    res.status(200).send(exams);
                })
           }); 
    } catch (error) {
        res.status(500).send(error)
    }
}
studentController.isExamActive = (req, res, next) => {
    try {
        const { examId } = req.params;
            ExamStatus.find({studentId: req.userData._id,examId: examId}).then(status => {
                res.status(200).send({isActive: status[0].isActive});
           }); 
    } catch (error) {
        res.status(500).send(error)
    }
}

studentController.getAcademicStudentInformation = (req, res, next) => {
    
    try {
        const { studentId } = req.params;
        Assessment.find({studentsId: studentId})
        .then(doc => {
            res.status(200).send({Assessments: doc});
        })
    } catch (error) {
        res.status(500).send(error)
    }
}

studentController.getStudentsFileAnswers = (req, res, next) => {
    try {
        const {examId} = req.params;
        StudentAnswers.find({ examId: examId }).then(answers => {
            return res.status(200).json({ answers: answers });
        }).catch(error => {
            return res.status(500).json(error);
        })
    } catch (error) {

    }
}
studentController.me = (req, res, next) => {
    const { student } = req;
    res.send({ student })
}
module.exports = studentController;