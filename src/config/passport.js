// const JwtStrategy = require('passport-jwt').Strategy;
// const ExtractJwt = require('passport-jwt').ExtractJwt;
// //const JWT_SECRET='HGSFKHSDGKHDFGJHG'
// const User = require('../models/teacher.model');

// module.exports = (passport) => {
//     let config = {};
//     config.secretOrKey = process.env.JWT_SECRET;
//     config.jwtFromRequest = ExtractJwt.fromAuthHeaderAsBearerToken();

//     passport.use(new JwtStrategy(config, async (jwtPayload, done) => {
//         try {
//             console.log(jwtPayload)
//             const user = await User.findById(jwtPayload._id);
//             if (user) {
//                 return done(null, user);
//             }else {
//                 return done(null, false);
//             }
//         }catch(e){
//             return done(err, false);
//         }
//     }));
// };

const jwt = require('jsonwebtoken');

module.exports = (req, res, next) => {
    try {
        const token = req.headers.authorization.split(" ")[1];
        // console.log(token)
        const decoded = jwt.verify(token, process.env.JWT_SECRET);
        req.userData = decoded;
        next();
    } catch (error) {
        console.log(error,"   error")
        return res.status(401).json({
            message: 'Auth failed'
        });
    }
};