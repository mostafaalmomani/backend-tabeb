const imageController = {};
const fs = require('fs')
const stream = require('stream');
const pathRes = require('path');


imageController.get = async (req, res, next) => {
  
    try {
        const { path } = req.params;
        const file = pathRes.resolve(__dirname + './../uploads/question/' + path);
        const r = fs.createReadStream(file) // or any other way to get a readable stream
        const ps = new stream.PassThrough() // <---- this makes a trick with stream error handling
        stream.pipeline(
         r,
         ps, // <---- this makes a trick with stream error handling
         (err) => {
          if (err) {
            console.log(err) // No such file or any other kind of error
            return res.sendStatus(400); 
          }
        })
        ps.pipe(res)
    } catch (e) {
      next(e);
    }
  };

  imageController.getStudentAnswers = async (req, res, next) => {
  
    try {
        const { path } = req.params; // /CMKRd4yzO4Enoq0jO4Jfahwi.pdf
        const file = pathRes.resolve(__dirname + './../uploads/answers/' + path);
        const r = fs.createReadStream(file) // or any other way to get a readable stream
        const ps = new stream.PassThrough() // <---- this makes a trick with stream error handling
        stream.pipeline(
         r,
         ps, // <---- this makes a trick with stream error handling
         (err) => {
          if (err) {
            console.log(err) // No such file or any other kind of error
            return res.sendStatus(400); 
          }
        })
        ps.pipe(res)
    } catch (e) {
      next(e);
    }
  };
module.exports = imageController;