const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const bodyParser = require('body-parser');
const passport = require('passport');
const cors = require('cors');
const path = require('path');
const assert = require('assert');
const v1 = require('./routes/v1');
const v2 = require('./routes/v2')
const session = require('express-session')
const cookieParser = require('cookie-parser')
require("./config/superadmin.create").createSuperAdmin();
const SimpleNodeLogger = require('simple-node-logger'),
	opts = {
		logFilePath:'server.log',
		timestampFormat:'YYYY-MM-DD HH:mm:ss.SSS'
	},
log = SimpleNodeLogger.createSimpleLogger( opts );
log.setLevel('warn');
log.setLevel('trace');
log.setLevel('debug');
log.setLevel('error');
log.setLevel('info');


const app = express();
var mongodb;
const fs = require('fs');
const expressLayout = require('express-ejs-layouts');
const multipart = require('connect-multiparty');
const multipartMiddleware = multipart({ uploadDir: __dirname + '/../src/uploads' });

 if(process.env.NODE_ENV === 'production') {

    var accessLogStream = fs.createWriteStream(__dirname  + "/../server.log", {flags: 'a'});
    app.use(logger({stream: accessLogStream}));
}
 else {
    app.use(logger("dev")); //log to console on development
}
//----------- DB Config -----------//
var connection = mongoose.connect(`${process.env.MONGODB_URI}`, {
    useNewUrlParser: true,
    useCreateIndex: true,
    poolSize: 10,
    useUnifiedTopology: true,
    useFindAndModify: false,
    failIndexKeyTooLong: false,
    setParameter: 1

}, function (err, db) {
    assert.equal(null, err);
    mongodb = db;
});



mongoose.connection.on('connected', () => {
    console.log('Connected to the database');
});
mongoose.connection.on('error', (err) => {
    console.error(`Failed to connected to the database: ${err}`);
});



app.use(expressLayout);
app.set('view engine', 'ejs');
// ----------- Middlewares -----------//
app.disable('etag');

app.use(cors({
    origin: "*",
    "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
    "preflightContinue": true,
    credentials: true,
    "Access-Control-Allow-Headers": "Origin, X-Requested-With, Content-Type, Accept, Authorization"

}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
// app.use(cors({ origin: process.env.CLIENT_URL }));
app.use(cookieParser(process.env.sessionSecret))
// require('./config/passport')(passport);


app.use(passport.initialize());
app.use(passport.session());


// const connection = mongoose.createConnection(process.env.MONGODB_URI, {
//     useNewUrlParser: true,
//     useCreateIndex: true,
//     poolSize: 10,
//     useUnifiedTopology: true

// });


//******************* passport-auth *****************************/
passport.serializeUser((user, cb) => {
    cb(null, user);
});

passport.deserializeUser((user, cb) => {
    cb(null, user);
});


// ----------- Routes -----------//
app.post('/api/upload', multipartMiddleware, (req, res) => {
    let filePath = __dirname + '\\uploads\\' + req.files.uploads[0].path.substr(req.files.uploads[0].path.lastIndexOf('\\'));
    const contents = fs.readFileSync(filePath, { encoding: 'base64' });
    res.send({
        image: contents
    });
})
app.use('/api/v1', v1);
app.use('/api/v2', v2);

// ----------- Static Files -----------//
// if (process.env.NODE_ENV === 'production') {
//     app.use(express.static(path.join(__dirname, '../../client/build')));
//     app.get('*', (req, res) => {
//         res.sendFile(
//             path.resolve(__dirname, '../../client', 'build', 'index.html')
//         );
//     });
// }

// ----------- ERRORS -----------//
app.use((req, res, next) => {
    //404 Not Found

    const err = new Error('Not Found');
    err.status = 404;
    next(err);
});

app.use((err, req, res, next) => {
    const status = err.status || 500;
    const error = err.message || 'Error processing your request';
    res.write('<style>     body {background-color: #92a8d1;}   .text { text-align: center;  font-family: \'Times New Roman\', Times, serif; color: red; margin: 300px auto }       </style>')
    res.write('<h1 class="btn btn-secondary text">' + status + ' ' + error + '</h1>')
    res.end();
    
});

module.exports = app;