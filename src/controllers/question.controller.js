const Questions = require('../models/questions.model');

const questionController = {};

questionController.get = async (req, res, next) => {
  const { user } = req.session;

  const now = new Date();

  const month = parseInt(req.params.month);
  {
    month >= 0 && month <= 11 && now.setMonth(month);
  }

  const firstDay = new Date(now.getFullYear(), now.getMonth(), 1);
  const lastDay = new Date(now.getFullYear(), now.getMonth() + 1, 0);

  const query = {
    owner: user._id,
    created: {
      $gte: firstDay,
      $lt: lastDay
    }
  };
  try {
    const question = await Question.find(query).sort({ created: 'desc' });
    const statistics = {};

    if (question.length > 0) {
      //Max amount spent in the specified month
      statistics.max = question.sort((a, b) => a.amount < b.amount)[0].amount;

      //Total amount spent in the specified month
      statistics.total = question
        .map(item => item.amount)
        .reduce((prev, next) => prev + next);

      //Avg question for the given month
      statistics.avg = Math.floor(statistics.total / question.length);
    }
    return res.send({
      question,
      statistics
    });
  } catch (e) {
    next(e);
  }
};

questionController.create = async (req, res, next) => {
  try {
    // TODO: // (windows \\ linux /)
    let filePaths = [];
    let filePath = '';
    if (req.files.image != undefined || req.files.image != null){
      var imageSize = JSON.parse(JSON.stringify(req.files.image)).length;
      if(imageSize>1) {
        req.files.image.forEach(element => {
          filePath =  element.path.substr(element.path.lastIndexOf('/')+1);
          filePaths.push({path: filePath, name: element.name});
          filePath = '';
        })
      } else {
        filePath =   req.files.image.path.substr(req.files.image.path.lastIndexOf('/')+1);
        filePaths.push({path: filePath, name:  req.files.image.name});
      }
    }
   
    const { point, examId, qousetionObject, rules } = req.body;
    const localQuestion = JSON.parse(qousetionObject);
    const localRules = rules!= null || rules != undefined?JSON.parse(rules):false;
    localQuestion.forEach(questionElement => {
      getImageQuestions(filePaths,localQuestion.indexOf(questionElement), function (result) {

      let correctAnswer = questionElement.answers.filter(ans =>  ans.answer === true);
      const newquestion = new Questions({
        questionText: questionElement.questionText,
        answers: questionElement.answers,
        correctAnswer: correctAnswer,
        point,
        examId,
        sequenceQ: localRules.isRandomSequence,
        imageQ: result,
      });

      newquestion.save().then(question => {
      }).catch(err => {
        console.log(err)
      });
    });
  })
    return res.send({
      question: [],
      message: 'saved'
    });
  } catch (e) {
    console.log(e)
    return res.send({
      err: e
    }) 
  }
};

questionController.update = async (req, res, next) => {
  const question_id = req.params.question_id;
  const { amount, description, created } = req.body;

  try {
    const check = await Questions.findOne({ _id: question_id });
    if (!check.owner.equals(req.user._id)) {
      const err = new Error('This question object does not belong to you!');
      err.status = 401;
      throw err;
    }

    const question = await Question.update(
      { _id: question_id },
      { amount, description, created }
    );
    return res.send({
      success: true,
      question
    });
  } catch (e) {
    next(e);
  }
};

questionController.destroy = async (req, res, next) => {
  const question_id = req.params.question_id;

  const check = await question.findOne({ _id: question_id });
  if (!check.owner.equals(req.user._id)) {
    const err = new Error('This question object does not belong to you!');
    err.status = 401;
    throw err;
  }

  try {
    await question.deleteOne({ _id: question_id });
    res.send({
      success: true
    });
  } catch (e) {
    next(e);
  }
};

questionController.getQuestions = async (req, res, next) => {
  try {
    const { examId } = req.params;
    Questions.find({examId: examId}).then(questions => {
      res.status(200).send(questions);
    }).catch(error => {
      res.status(500).send(error);
    })
    
  } catch (error) {
    res.status(500).send(error);
  }
}
function getImageQuestions(files, index, callback) {
  let filePath = files.filter(f => Number(f.name) === index);
  callback(filePath.length === 0?'':filePath[0].path);
}
module.exports = questionController;