const mongoose = require('mongoose');

const { Schema } = mongoose;

const ConfirmSchema = Schema({
  isConfirmed: { type: Boolean, required: false },
  teacherId: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Teacher' },
  courseNumber: { type: String, required: false },
  courseName: { type: String, required: false },
  teacherName: { type: String, required: false },
  created: { type: Date, default: new Date() }
});

const Confirm = mongoose.model('Confirm', ConfirmSchema);
module.exports = Confirm;