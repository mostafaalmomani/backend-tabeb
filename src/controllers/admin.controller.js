const jwt = require('jsonwebtoken');
const Admin = require("../models/admin.model");
const Student = require("../models/teacher.model");
const Exam = require("../models/exam.model");
const Course = require("../models/course.model");
const Teacher = require("../models/teacher.model");
const MarkCourse = require("../models/mark.course.model");
const ConfirmSchema = require('../models/confirm.request.model');
const Mark = require('../models/mark.model');
const Rules = require('../models/rules.model');
const Questions = require('../models/questions.model');
const ExamStatus = require('../models/exam.status.model');
const Assessment = require('../models/assessment.course');
const StudentAnswers = require('../models/answersFile.model');

const sgMail = require('@sendgrid/mail');
const { validationResult } = require('express-validator');
const _ = require('lodash');
const fs = require('fs');
const SENDGRID_API_KEY = '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'

const adminController = {};

adminController.regisetr = async (req, res, next) => {
    try {
        const { fullName, email, password, phoneNumber,course_id } = req.body;
        const newteacher = new Teacher({
            fullName,
            email,
            password,
            purePassword: password,
            course_id,
            phoneNumber,
            joined: (new Date).getDate()
        });
        newteacher.save().then(savedTeacher => {
            return res.send({ message: 'saved!' });
        })
    } catch (e) {
        console.log(e)
        if (e.code === 11000 && e.name === 'MongoError') {
            const error = new Error(`Email address ${newteacher.email} is already taken`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};

adminController.update = async (req, res, next) => {
    try {
        const { fullName, email, password, phoneNumber,course_id,_id } = req.body.user;
        Teacher.findByIdAndUpdate({_id:_id}, 
            {
             fullName:fullName,
             email:email,
             phoneNumber:phoneNumber,
             $push: { course_id: course_id }
            }).then(doc => {
            return res.send({ message: "saved!" });
        }).catch(e => {
            console.log(e)
            return res.status(500).send({ e: e });
        });

    } catch (e) {
        console.log(e)
        if (e.code === 11000 && e.name === 'MongoError') {
            const error = new Error(`Email or Number  ${req.body.teacher.email} is already taken`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};

adminController.login = async (req, res, next) => {   
    try {
        const { email, password } = req.body;
        const teacher = await Admin.findOne({ email });
        if (!teacher) {
            const err = new Error(`The email ${email} was not found on our system`);
            err.status = 401;
            return next(err);
        }

        //Check the password
        teacher.isPasswordMatch(password, teacher.password, (err, matched) => {
            if (matched) { //Generate JWT
                const secret = process.env.JWT_SECRET;
                const expire = process.env.JWT_EXPIRATION;

                const token = jwt.sign({ _id: teacher._id }, secret, { expiresIn: expire });
                res.cookie('Adminid',teacher._id)
                return res.send({
                    id: teacher._id,
                    teachername: teacher.email,
                    fullName: teacher.fullName,
                    token: token,
                    expiresIn: 3600
                });
            }
            res.status(401).send({
                error: 'Invalid email/password combination'
            });

        });

    } catch (e) {
        next(e);
    }

};
adminController.forgetPassword = async (req, res, next) => {
    const { email } = req.body;
    const teacher = await teacher.findOne({ email });
    const secret = process.env.JWT_SECRET;
    const expire = process.env.JWT_EXPIRATION;
    const token = jwt.sign({ _id: teacher._id }, secret, { expiresIn: '5m' });
    sgMail.setApiKey(SENDGRID_API_KEY);
    const msg = {
        to: email,
        from: 'mostafaalmomani98@gmail.com',
        subject: `Password Reset link`,
        html: `
                    <h1 style="color: #61dafb">Please use the following link to reset your password</h1>
                    <p style="color: #61dafb">${process.env.CLIENT_URL}/teachers/password/reset/${token}</p>
                    <hr />
                    <p style="color: #61dafb">This email may contain sensetive information</p>
                    <p style="color: #61dafb">${process.env.CLIENT_URL}</p>
                `,
    };
    return Teacher.updateOne(
        {
            resetPasswordLink: token
        },
        (err, success) => {
            if (err) {
                console.log('RESET PASSWORD LINK ERROR', err);
                return res.status(400).json({
                    error:
                        'Database connection error on teacher password forgot request'
                });
            } else {
                sgMail
                    .send(msg)
                    .then(sent => {
                        // console.log('SIGNUP EMAIL SENT', sent)
                        return res.json({
                            message: `Email has been sent to ${email}. Follow the instruction to activate your account`
                        });
                    })
                    .catch(err => {
                        // console.log('SIGNUP EMAIL SENT ERROR', err)
                        return res.json({
                            message: err.message
                        });
                    });
            }
        }
    );
    // return res.send({ message: 'check your email to reset your password' });

}

adminController.resetPassword = (req, res, next) => {
    const { resetPasswordLink, newPassword } = req.body;
    console.log(resetPasswordLink)
    const errors = validationResult(req);

    if (!errors.isEmpty()) {
        const firstError = errors.array().map(error => error.msg)[0];
        return res.status(422).json({
            errors: firstError
        });
    } else {
        if (resetPasswordLink) {
            jwt.verify(resetPasswordLink, process.env.JWT_SECRET, function (
                err,
                decoded
            ) {
                if (err) {
                    return res.status(400).json({
                        error: 'Expired link. Try again'
                    });
                }
                Teacher.findOne({ resetPasswordLink }).then(teacher => {
                });
                Teacher.findOne(
                    {
                        resetPasswordLink
                    },
                    (err, teacher) => {
                        if (err || !teacher) {
                            return res.status(400).json({
                                error: 'Something went wrong. Try later'
                            });
                        }

                        const updatedFields = {
                            password: newPassword,
                            resetPasswordLink: ''
                        };

                        teacher = _.extend(teacher, updatedFields);

                        teacher.save((err, result) => {
                            if (err) {
                                return res.status(400).json({
                                    error: 'Error resetting teacher password'
                                });
                            }
                            res.json({
                                message: `Great! Now you can login with your new password`
                            });
                        });
                    }
                );
            });
        }
    }
}
adminController.me = (req, res, next) => {
    const { teacher } = req.session;
    const contents = fs.readFileSync(filePath, { encoding: 'base64' });
    return res.status(200).json({ teacher })
}

adminController.getCardInformation = async (req, res, next) => {
    const teachers = await Teacher.find({}).count();
    const exams = await Exam.find({}).count();
    const students = await Student.find({}).count();
    const courses = await Course.find({}).count();
    return res.status(200).json({ teachers: teachers, exams: exams, students: students, courses: courses });

}
adminController.getCourses = async (req, res, next) => {
    Course.find().then(courses => {
        return res.status(200).json({courses: courses});
    });
}
adminController.getTeachers = async (req, res, next) => {
    Teacher.find().then(teachers => {
        return res.status(200).json({teachers: teachers});
    });
}

adminController.delete = async (req, res, next) => {
    try {
        const { teacher_id } = req.params;
        Teacher.findByIdAndDelete({ _id: teacher_id }).then(doc => {
            return res.send({ message: "deleted!" });
        }).catch(e => {
            console.log(e)
            return res.status(500).send({ e: e });
        });

    } catch (e) {
        console.log(e)
        if (e) {
            const error = new Error(`something error!!!`);
            error.status = 400
            next(error);
        } else {
            next(e);
        }

    }

};

adminController.confirm = async (req, res, next) => {

    const {course} = req.body;
    try {
        MarkCourse.updateMany({ teacherId: course.teacherId, courseNumber: course.courseNumber },
            {isConfirmed: true},{new:true, upsert: true}).then(doc => {
                ConfirmSchema.findByIdAndUpdate({_id: course._id}, {isConfirmed: true},
                    {new:true, upsert: true}).then(doc => {
                    return res.status(200).json({ Confirmed: true });
                }).catch(error => {
                    return res.status(500).json(error);
                }) 
        }).catch(error => {
            return res.status(500).json(error);
        })    
    } catch (error) {
        return res.status(500).json(error);
    }
       
}

adminController.getRequests = async (req, res, next) => {

    ConfirmSchema.find({}).then(requests => {
        return res.status(200).json({ requests: requests });
    }).catch(error => {
        return res.status(500).json(error);
    })
    
}

adminController.deleteAllData = async (req, res, next) => {
    try {
        Exam.deleteMany().then(doc => {
            console.log("Exam deleted successfully", doc)
        });
        ExamStatus.deleteMany().then(doc => {
            console.log("ExamStatus deleted successfully", doc)
        });
        Assessment.deleteMany().then(doc => {
            console.log("Assessment deleted successfully", doc)
        });
        MarkCourse.deleteMany().then(doc => {
            console.log("MarkCourse deleted successfully", doc)
        });
        Mark.deleteMany().then(doc => {
            console.log("Mark deleted successfully", doc)
        });
        StudentAnswers.deleteMany().then(doc => {
            console.log("StudentAnswers deleted successfully", doc)
        });
        Questions.deleteMany().then(doc => {
            console.log("Questions deleted successfully", doc)
        });
        Rules.deleteMany().then(doc => {
            console.log("Rules deleted successfully", doc)
        });
        ConfirmSchema.deleteMany().then(doc => {
            console.log("ConfirmSchema deleted successfully", doc)
        });
        return res.status(200).json({ message: "done" });
    } catch (error) {
        return res.status(500).json(error);
    }
    
}

adminController.deleteCourse = async (req, res, next) => {
    try {
        const {courseId} = req.params;
        Course.findByIdAndDelete({_id: courseId}).then(doc => {
            return res.status(200).json({ message: "done" });
        });
    } catch (error) {
        return res.status(500).json(error);
    }
    
}
module.exports = adminController;