
const Exam = require('../models/exam.model');
const Question = require('../models/questions.model');
const Student = require('../models/student.model');

const testBankController = {};


testBankController.create = async (req, res, next) => {

    try {
        // console.log(req.body.students)
        const { examType, courseName, questionsCount,
            semester, courseNumber, year, examDate, start, end, duration, maxGreade,markForEachQ } = req.body.exam;
        const newExam = new Exam({
            examType: 6,
            courseName,
            questionsCount,
            semester,
            courseNumber,
            year,
            examDate,
            duration,
            maxGreade,
            markForEachQ,
            teacherId: req.userData._id,
        });
       
        newExam.save().then(exam => {
          req.body.students.forEach(student => {
            Exam.findByIdAndUpdate(exam._id, {
                $push: { studentsId: student._id }
            }).then(exam => {
            });
          });
          res.status(200).json({message: 'saved', exam: exam });
        });

    } catch (error) {
        console.log(error)
        res.status(500).json({ error: error })
    }
};

testBankController.createQ = async (req, res, next) => {
    try {
   // TODO: // windows \\ linux /
      let filePaths = [];
      let filePath = '';
      if (req.files.image != undefined || req.files.image != null){
        var imageSize = JSON.parse(JSON.stringify(req.files.image)).length;
        if(imageSize>1) {
          req.files.image.forEach(element => {
            filePath =  element.path.substr(element.path.lastIndexOf('/')+1);
            filePaths.push({path: filePath, name: element.name});
            filePath = '';
          })
        } else {
          filePath =   req.files.image.path.substr(req.files.image.path.lastIndexOf('/')+1);
          filePaths.push({path: filePath, name:  req.files.image.name});
        }
      }
      
 
      const {  examId, qousetionObject, explanation, rules } = req.body;
      const localQuestion = JSON.parse(qousetionObject);
      const expObj = JSON.parse(explanation)
      let i = 0;
      localQuestion.forEach(questionElement => {
        let correctAnswer = questionElement.answers.filter(ans =>  ans.answer === true);
        getImageQuestions(filePaths,localQuestion.indexOf(questionElement), function (result) {
          const newquestion = new Question({
            questionText: questionElement.questionText,
            answers: questionElement.answers,
            correctAnswer: correctAnswer,
            explanation: expObj[i] != undefined ? expObj[i] : undefined,
            examId,
            questionType: 'test',
            sequenceQ: false,
            imageQ: result
          });

          newquestion.save().then(question => {
            // console.log(question);
          }).catch(err => {
            console.log(err)
          });
        });
        i++;
      })
      
      return res.status(200).send({
        question: null,
        message: 'saved'
      });

    } catch (e) {
      console.log(e)
      return res.status(500).send({
          err: e
        })
    }
  };

  testBankController.getExamsForTestBank = async(req, res, next) =>{
    try {

      const  studentId  = [];
      studentId.push(req.query.studentID);
      Exam.find({ examType : 6 }).where('studentsId').in(studentId).then(exams => {
          res.status(200).json(exams);
      });
    } catch (error) {
      console.log(error)
        res.status(500).json({ error: error })
    }
   
  }

  testBankController.getQuestionsForExam = async(req, res, next)=>{

    Question.find({examId: req.query.examID}).then(data=>{
      res.json(data.map(e=>e._id));
    });
  }

  testBankController.getQuestion = async(req, res, next)=>{
    Question.find({_id: req.query.qID}).then(data =>{
      res.json(data);
    });
  }

  testBankController.saveAnswerForQuestion = async(req, res, next)=>{
    Question.findOneAndUpdate({_id: req.body.question},{ $set:{ answers: req.body.answer },$inc: {total: 1} }, {returnOriginal:false}).then(data=>{
      res.json(data);
    })
  }

 function getImageQuestions(files, index, callback) {
    let filePath = files.filter(f => Number(f.name) === index);
    callback(filePath.length === 0?'':filePath[0].path);
  }
  
module.exports = testBankController;