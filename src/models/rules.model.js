const mongoose = require('mongoose');

const { Schema } = mongoose;

const RulesSchema = Schema({
  returnBack: { type: Boolean, required: false },
  isRandomSequence: { type: Boolean, required: false},
  numberOfPage: { type: Number, required: false},
  examLanguage: { type: String, required: false},
  resultVisable: { type: Boolean, required: false},
  incorrectAnswersIsVisable: { type: Boolean, required: false},
  isRequiredQusetions: { type: Boolean, required: false},
  // resultVisable: { type: Number, required: false},
  created: { type: Date, default: new Date() }
});

const Rules = mongoose.model('Rules', RulesSchema);
module.exports = Rules;