const { strict } = require('assert');
const mongoose = require('mongoose');
var autoIncrement = require('mongoose-auto-increment');
autoIncrement.initialize(mongoose.connection);
const { Schema } = mongoose;

const ExamSchema = Schema({
    examType: { type: Number, required: true, default: -1 },
    duration: { type: String, required: false, default: ''  }, 
    start: { type: Date, required: false, default: new Date  },
    end: { type: Date, required: false, default: new Date  },
    semester: { type: String, required: false, default: ''  },
    year: { type: String, required: false, default: ''  },
    questionsCount: { type: String, required: false, default: '' },
    isActive: { type: Boolean, required: false ,default: true},
    questions: { type: [mongoose.Schema.Types.ObjectId], required: false, ref: 'Questions' },
    rulesId: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Rules' },
    courseName: { type: String, required: true },
    courseNumber: { type: String, required: true },
    scale: {type: [Map], unique: false, required: false, of: [String]},
    TimeStamp: {type: Date, required: false},
    created: { type: Date, default: new Date() },
    examDate:{type: Date, default: new Date()},
    tokenLink: { type: String, required: false },
    maxGreade: { type: Number, required: false},
    markForEachQ: {type: Number, required: false},
    teacherId: { type: mongoose.Schema.Types.ObjectId, required: false, ref: 'Teacher' },
    studentsId: { type: [mongoose.Schema.Types.ObjectId], required: false, index: {unique: false}, ref: 'Student' },
});

ExamSchema.plugin(autoIncrement.plugin,  {
    model: 'Exam',
    field: 'examNumber',
    startAt: (new Date()).getUTCFullYear()+200,
    incrementBy: 2
})
const Exam = mongoose.model('Exam', ExamSchema);
module.exports = Exam;